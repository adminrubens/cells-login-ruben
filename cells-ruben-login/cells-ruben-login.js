{
  const {
    html,
  } = Polymer;
  /**
    `<cells-ruben-login>` Description.

    Example:

    ```html
    <cells-ruben-login></cells-ruben-login>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-ruben-login | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsRubenLogin extends Polymer.Element {

    static get is() {
      return 'cells-ruben-login';
    }

    static get properties() {
      return {
        prop1: {
          type: String,
          value: 'loginruben-element'
        },
        ocultar: {
          type: Boolean,
          value: false
        },
        propiedad: {
          type: Boolean,
          value: "false",
          notify: true
        }

      };
    }
    login(){
      if (this.$.user.value === "user"&& this.$.pass.value === 'Rubens'){
        this.$.messageerror.hidden = true;
        this.$.messageok.hidden = false;
        this.set('propiedad', true)
      }
      else{
        this.$.messageok.hidden = true;
        this.$.messageerror.hidden = false;
      }
    }
    static get template() {
      return html `
      <style include="cells-ruben-login-styles cells-ruben-login-shared-styles"></style>
      <slot></slot>
      
          <p>Bienvenido a Cells</p>
          <div class="centrar">
      <form id="myform" name=form action="TU_PAGINA_WEB.HTML">
        <P>Usuario:    <input type=text id="user" name=login placeholder="Usuer">
        <P>Contraseña: <input id="pass" type=password name=password placeholder="Password">
          <button on-click="login" class="acceder btn"  type="button">Acceder</button>
        <p id="messageok" hidden="[[!ocultar]]">Acceso correcto</p>
        <p id="messageerror" hidden="[[!ocultar]]">Datos Incorrectos intente de nuevo</p>
      </form>
    </div>
      
      `;
    }
  }

  customElements.define(CellsRubenLogin.is, CellsRubenLogin);
}
